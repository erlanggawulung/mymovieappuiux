package com.example.mymovieappuiux.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.mymovieappuiux.BuildConfig;

import org.json.JSONObject;

public class Movie implements Parcelable {
    private String id;
    private String title;
    private String posterUrl;
    private String rating;
    private String duration;
    private String tagline;
    private String language;
    private String release;
    private String description;
    private String genres;

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public Movie() {

    }

    public Movie(String[] array) {
        try {
            this.id = array[0];
            this.title = array[1];
            this.description = array[2];
            this.posterUrl = array[3];
            this.rating = array[4];
            this.duration = array[5];
            this.tagline = array[6];
            this.language = array[7];
            this.release = array[8];
            this.genres = array[9];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Movie(JSONObject object) {
        try {
            int id = object.getInt("id");
            String title = object.getString("title");
            String description = object.getString("overview");
            String posterUrl = object.getString("poster_path");

            this.id = Integer.toString(id);
            this.title = title;
            this.description = description;
            this.posterUrl = BuildConfig.IMAGE_URL + posterUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Movie(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.posterUrl = in.readString();
        this.rating = in.readString();
        this.duration = in.readString();
        this.tagline = in.readString();
        this.language = in.readString();
        this.release = in.readString();
        this.genres = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.posterUrl);
        dest.writeString(this.rating);
        dest.writeString(this.duration);
        dest.writeString(this.tagline);
        dest.writeString(this.language);
        dest.writeString(this.release);
        dest.writeString(this.genres);
    }
}

package com.example.mymovieappuiux.activities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mymovieappuiux.R;
import com.example.mymovieappuiux.helpers.DetailsTaskLoader;
import com.example.mymovieappuiux.models.Movie;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Movie>{
    static final String EXTRAS_MOVIE_ID = "EXTRAS_MOVIE_ID";
    public static String EXTRA_MOVIE_DETAILS = "EXTRA_MOVIE_DETAILS";
    @BindView(R.id.tv_title_details) TextView movieTitle;
    @BindView(R.id.img_poster_details) ImageView imagePoster;
    @BindView(R.id.tv_tagline_details) TextView movieTagline;
    @BindView(R.id.tv_rating_value) TextView movieRating;
    @BindView(R.id.tv_duration_value) TextView movieDuration;
    @BindView(R.id.tv_language_value) TextView movieLanguage;
    @BindView(R.id.tv_genres_value) TextView movieGenres;
    @BindView(R.id.tv_release_date) TextView movieReleaseDate;
    @BindView(R.id.tv_overview_value) TextView movieDescription;
    @BindView(R.id.progress_bar_details) ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        Movie item = getIntent().getParcelableExtra(EXTRA_MOVIE_DETAILS);
        String movieId = item.getId();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_MOVIE_ID, movieId);
        getSupportLoaderManager().initLoader(0, bundle, this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(item.getTitle());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public Loader<Movie> onCreateLoader(int i, @Nullable Bundle bundle) {
        String movieId = "";
        if (bundle != null) {
            movieId = bundle.getString(EXTRAS_MOVIE_ID);
        }
        return new DetailsTaskLoader(this, movieId);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Movie> loader, Movie movieDetails) {
        movieTitle.setText(movieDetails.getTitle());
        Glide.with(this)
                .load(movieDetails.getPosterUrl())
                .into(imagePoster);
        movieTagline.setText(movieDetails.getTagline());
        movieRating.setText(movieDetails.getRating());
        movieDuration.setText(movieDetails.getDuration());
        movieLanguage.setText(movieDetails.getLanguage());
        movieGenres.setText(movieDetails.getGenres());
        movieReleaseDate.setText(movieDetails.getRelease());
        movieDescription.setText(movieDetails.getDescription());
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Movie> loader) {
        Log.d(DetailsActivity.class.getSimpleName(), "onLoadReset");
    }
}

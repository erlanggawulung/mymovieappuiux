package com.example.mymovieappuiux.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mymovieappuiux.fragments.NowShowingFragment;
import com.example.mymovieappuiux.R;
import com.example.mymovieappuiux.fragments.UpcomingFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final String STATE_TITLE = "state_string";
    @BindView(R.id.btn_now_showing)
    Button btnNowShowing;
    @BindView(R.id.btn_upcoming)
    Button btnUpcoming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        FragmentManager mfragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mfragmentManager.beginTransaction();
        NowShowingFragment mNowShowingFragment = new NowShowingFragment();

        Fragment fragment = mfragmentManager.findFragmentByTag(NowShowingFragment.class.getSimpleName());


        btnNowShowing.setOnClickListener(this);
        btnUpcoming.setOnClickListener(this);

        if (savedInstanceState == null) {
            if (!(fragment instanceof NowShowingFragment)) {
                mFragmentTransaction.replace(R.id.frame_container, mNowShowingFragment, NowShowingFragment.class.getSimpleName());
                mFragmentTransaction.commit();
            }
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(btnNowShowing.getText());
            }
        } else {
            String stateTitle = savedInstanceState.getString(STATE_TITLE);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(stateTitle);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TITLE, getSupportActionBar().getTitle().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings){
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_now_showing) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(btnNowShowing.getText());
            }

            FragmentManager mFragmentManager = getSupportFragmentManager();

            if (mFragmentManager != null) {
                NowShowingFragment mNowShowingFragment = new NowShowingFragment();
                FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
                Fragment fragment = mFragmentManager.findFragmentByTag(NowShowingFragment.class.getSimpleName());

                if (!(fragment instanceof NowShowingFragment)) {
                    mFragmentTransaction.replace(R.id.frame_container, mNowShowingFragment, NowShowingFragment.class.getSimpleName());
                    mFragmentTransaction.commit();
                }
            }
        } else if (v.getId() == R.id.btn_upcoming) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(btnUpcoming.getText());
            }

            FragmentManager mFragmentManager = getSupportFragmentManager();

            if (mFragmentManager != null) {
                UpcomingFragment mUpcomingFragment = new UpcomingFragment();
                FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
                Fragment fragment = mFragmentManager.findFragmentByTag(UpcomingFragment.class.getSimpleName());

                if (!(fragment instanceof UpcomingFragment)) {
                    mFragmentTransaction.replace(R.id.frame_container, mUpcomingFragment, UpcomingFragment.class.getSimpleName());
                    mFragmentTransaction.commit();
                }
            }
        }
    }
}

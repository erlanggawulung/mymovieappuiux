package com.example.mymovieappuiux.data;

import com.example.mymovieappuiux.models.Movie;

import java.util.ArrayList;

public class UpcomingData {
    public static String[][] data = new String[][] {
        {
            "1","Justice League","https://image.tmdb.org/t/p/w185/eifGNCSDuxJeS1loAXil5bIGgvC.jpg",
            "6.2","120","","EN","2017-11-15",
            "Fuelled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.",
            "Action, Drama, Comedy"
        },
        {
            "2","Justice League: Doom","https://image.tmdb.org/t/p/w185/4KIk5Odvkpwlf5y0QYqohTTlujw.jpg",
            "7.3","120","","EN","2012-02-28",
            "An adaptation of Mark Waid's \\\"Tower of Babel\\\" story from the JLA comic. Vandal Savage steals confidential files Batman has compiled on the members of the Justice League, and learns all their weaknesses.",
            "Action, Drama, Comedy"
        },
        {
            "3","Justice League Dark","https://image.tmdb.org/t/p/w185/lGa2sK2LdcHQjasYyukl3P2nras.jpg",
            "6.9","160","","EN","2012-02-28",
            "Beings with supernatural powers join together to fight against supernatural villains.",
            "Action, Drama, Comedy"
        },
        {
            "4","Justice League: Throne of Atlantis","https://image.tmdb.org/t/p/w185/3n0zmm2dPF0TQt2ikJ2tUjKr2FT.jpg",
            "6.7","120","","EN","2015-01-27",
            "After the events of Justice League: War, Ocean Master and Black Manta have declared a war against the surface in retaliation of the aftermath of Apokoliptian-tyrant Darkseid's planetary invasion. Queen Atlanna seeks out her other son, Ocean Master’s half-brother Arthur Curry, a half-human with aquatic powers with no knowledge of his Atlantean heritage, to restore balance.",
            "Action, Drama, Comedy"
        },
        {
            "5","Justice League: War","https://image.tmdb.org/t/p/w185/11X9WnPuSCnKavfTe0AJ935MDl4.jpg",
            "7.2","120","","EN","2014-02-02",
            "The world is under attack by an alien armada led by the powerful Apokoliptian, Darkseid. A group of superheroes consisting of Superman, Batman, Wonder Woman, The Flash, Green Lantern, Cyborg, and Shazam must set aside their differences and gather together to defend Earth.",
            "Action, Drama, Comedy"
        },
    };

    public static ArrayList<Movie> getListData(){
        ArrayList<Movie> list = new ArrayList<>();
        for (String[] aData : data) {
            Movie movie = new Movie(aData);
            movie.setId(aData[0]);
            movie.setTitle(aData[1]);
            movie.setPosterUrl(aData[2]);
            movie.setRating(aData[3]);
            movie.setDuration(aData[4]);
            movie.setTagline(aData[5]);
            movie.setLanguage(aData[6]);
            movie.setRelease(aData[7]);
            movie.setDescription(aData[8]);
            movie.setGenres(aData[9]);
            list.add(movie);
        }
        return list;
    }
}

package com.example.mymovieappuiux.data;

import com.example.mymovieappuiux.models.Movie;

import java.util.ArrayList;

public class NowShowingData {
    public static String[][] data = new String[][] {
        {
            "1","The Avengers","https://image.tmdb.org/t/p/w185/cezWGskPY5x7GaglTTRN4Fugfb8.jpg",
            "7.6","120","","EN","2012-04-25",
            "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
            "Action, Drama"
        },
        {
            "2","Avengers: Endgame","https://image.tmdb.org/t/p/w185/bJLYrLIHT1r7cikhWGbpZkxlUpA.jpg",
            "0","180","","EN","2019-04-24",
            "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
            "Action, Drama"
        },
        {
            "3","Avengers: Infinity War","https://image.tmdb.org/t/p/w185/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
            "8.3","150","","EN","2018-04-25",
            "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality.",
            "Action, Drama"
        },
        {
            "4","Avengers: Age of Ultron","https://image.tmdb.org/t/p/w185/t90Y3G8UGQp0f0DrP60wRu9gfrH.jpg",
            "7.3","140","","EN","2015-04-22",
            "When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and Earth’s Mightiest Heroes are put to the ultimate test as the fate of the planet hangs in the balance. As the villainous Ultron emerges, it is up to The Avengers to stop him from enacting his terrible plans, and soon uneasy alliances and unexpected action pave the way for an epic and unique global adventure.",
            "Action, Drama"
        },
        {
            "5","Iron Man","https://image.tmdb.org/t/p/w185/s2IG9qXfhJYxIttKyroYFBsHwzQ.jpg",
            "7.5","110","","EN","2008-04-30",
            "After being held captive in an Afghan cave, billionaire engineer Tony Stark creates a unique weaponized suit of armor to fight evil.",
            "Action, Drama"
        },
        {
            "6","Iron Man 3","https://image.tmdb.org/t/p/w185/1Ilv6ryHUv6rt9zIsbSEJUmmbEi.jpg",
            "6.9","110","","EN","2013-04-18",
            "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts an odyssey of rebuilding and retribution.",
            "Action, Drama"
        }
    };

    public static ArrayList<Movie> getListData(){
        ArrayList<Movie> list = new ArrayList<>();
        for (String[] aData : data) {
            Movie movie = new Movie(aData);
            movie.setId(aData[0]);
            movie.setTitle(aData[1]);
            movie.setPosterUrl(aData[2]);
            movie.setRating(aData[3]);
            movie.setDuration(aData[4]);
            movie.setTagline(aData[5]);
            movie.setLanguage(aData[6]);
            movie.setRelease(aData[7]);
            movie.setDescription(aData[8]);
            movie.setGenres(aData[9]);
            list.add(movie);
        }
        return list;
    }
}

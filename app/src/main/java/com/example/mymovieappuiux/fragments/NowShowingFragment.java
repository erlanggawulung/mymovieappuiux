package com.example.mymovieappuiux.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.mymovieappuiux.R;
import com.example.mymovieappuiux.adapters.ListNowShowingAdapter;
import com.example.mymovieappuiux.adapters.ListUpcomingAdapter;
import com.example.mymovieappuiux.data.NowShowingData;
import com.example.mymovieappuiux.helpers.NowShowingTaskLoader;
import com.example.mymovieappuiux.models.Movie;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class NowShowingFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>{
    @BindView(R.id.rv_now_showing)
    RecyclerView rvNowShowing;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private ArrayList<Movie> list = new ArrayList<>();
    ListNowShowingAdapter listMovieAdapter;

    public NowShowingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_now_showing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        rvNowShowing.setHasFixedSize(true);

        //list.addAll(NowShowingData.getListData());
        showRecyclerList();

        progressBar.setVisibility(View.GONE);
        getLoaderManager().initLoader(0, savedInstanceState, this);
    }

    private void showRecyclerList(){
        rvNowShowing.setLayoutManager(new LinearLayoutManager(getActivity()));
        listMovieAdapter = new ListNowShowingAdapter(getActivity());
        listMovieAdapter.setListMovie(list);
        rvNowShowing.setAdapter(listMovieAdapter);
    }

    @NonNull
    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int i, @Nullable Bundle bundle) {
        progressBar.setVisibility(View.VISIBLE);
        return new NowShowingTaskLoader(getActivity());
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        progressBar.setVisibility(View.GONE);
        listMovieAdapter.setListMovie(movies);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<Movie>> loader) {
        progressBar.setVisibility(View.GONE);
    }
}

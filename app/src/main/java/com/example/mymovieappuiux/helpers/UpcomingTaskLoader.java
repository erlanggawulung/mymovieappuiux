package com.example.mymovieappuiux.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.mymovieappuiux.BuildConfig;
import com.example.mymovieappuiux.models.Movie;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class UpcomingTaskLoader extends AsyncTaskLoader<ArrayList<Movie>> {
    private static final String API_KEY = BuildConfig.TMDB_API_KEY;
    private ArrayList<Movie> mData;
    private boolean mHasResult = false;

    public UpcomingTaskLoader(@NonNull Context context) {
        super(context);

        onContentChanged();
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        } else if (mHasResult) {
            deliverResult(mData);
        }
    }

    @Override
    public void deliverResult(final ArrayList<Movie> data) {
        mData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResources(mData);
            mData = null;
            mHasResult = false;
        }
    }

    @Nullable
    @Override
    public ArrayList<Movie> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<Movie> movieItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL + "/movie/upcoming?api_key=" + API_KEY + "&language=en-US";
        Log.d(NowShowingTaskLoader.class.getSimpleName(), url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray results = responseObject.getJSONArray("results");

                    for (int i = 0; i < results.length(); i++) {
                        JSONObject movieJson = results.getJSONObject(i);
                        Movie movie = new Movie(movieJson);
                        movieItems.add(movie);
                    }
                    Log.d(NowShowingTaskLoader.class.getSimpleName(), result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d(NowShowingTaskLoader.class.getSimpleName(), "onFailure");
            }
        });
        return movieItems;
    }

    protected void onReleaseResources(ArrayList<Movie> data) {
        // do nothing
    }

}

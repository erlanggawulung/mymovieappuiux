package com.example.mymovieappuiux.helpers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.mymovieappuiux.BuildConfig;
import com.example.mymovieappuiux.models.Movie;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class DetailsTaskLoader extends AsyncTaskLoader<Movie> {
    private static final String API_KEY = BuildConfig.TMDB_API_KEY;
    private Movie movieDetails;
    private boolean mHasResult = false;
    private String movieId;

    public DetailsTaskLoader(final Context context, String movieId) {
        super(context);

        onContentChanged();
        this.movieId = movieId;
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        } else if (mHasResult) {
            deliverResult(movieDetails);
        }
    }

    @Override
    public void deliverResult(final Movie movieDetails) {
        this.movieDetails = movieDetails;
        mHasResult = true;
        super.deliverResult(movieDetails);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResources(this.movieDetails);
            this.movieDetails = null;
            this.mHasResult = false;
        }
    }

    @Nullable
    @Override
    public Movie loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        String url = BuildConfig.BASE_URL + "/movie/" + this.movieId + "?api_key=" + API_KEY;
        final Movie movieDetails = new Movie();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);

                    String title = responseObject.getString("title");
                    String posterUrl = responseObject.getString("poster_path");

                    double rating = responseObject.getDouble("vote_average");
                    int duration = responseObject.getInt("runtime");
                    String tagline = responseObject.getString("tagline");
                    String language = responseObject.getString("original_language");
                    String release = responseObject.getString("release_date");
                    String description = responseObject.getString("overview");
                    JSONArray genresArray = responseObject.getJSONArray("genres");
                    String basicPosterUrl = BuildConfig.IMAGE_URL;
                    String genres = "";
                    for (int i = 0; i < genresArray.length(); i++) {
                        if (i == 0) {
                            genres = genresArray.getJSONObject(i).getString("name");
                        } else {
                            genres += ", " + genresArray.getJSONObject(i).getString("name");
                        }
                    }

                    movieDetails.setTitle(title);
                    movieDetails.setPosterUrl(basicPosterUrl + posterUrl);
                    movieDetails.setRating(Double.toString(rating));
                    movieDetails.setDuration(Integer.toString(duration));
                    movieDetails.setTagline(tagline);
                    movieDetails.setLanguage(language);
                    movieDetails.setRelease(release);
                    movieDetails.setGenres(genres);
                    movieDetails.setDescription(description);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("MovieDetailsTaskLoader", "onFailure");
            }
        });

        return movieDetails;
    }

    protected void onReleaseResources(Movie movieDetails) {
        // do nothing
    }
}

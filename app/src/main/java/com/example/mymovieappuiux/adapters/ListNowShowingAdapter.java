package com.example.mymovieappuiux.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.mymovieappuiux.activities.DetailsActivity;
import com.example.mymovieappuiux.models.Movie;
import com.example.mymovieappuiux.R;

import java.util.ArrayList;

public class ListNowShowingAdapter extends RecyclerView.Adapter<ListNowShowingAdapter.NowShowingViewHolder> {
    private Context context;
    private ArrayList<Movie> listMovie;

    public ListNowShowingAdapter(Context context) {
        this.context = context;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public ArrayList<Movie> getListMovie() {
        return listMovie;
    }
    public void setListMovie(ArrayList<Movie> listMovie) {
        this.listMovie = listMovie;
    }

    @NonNull
    @Override
    public NowShowingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_now_showing, viewGroup, false);
        return new NowShowingViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull NowShowingViewHolder nowShowingViewHolder, final int i) {
        nowShowingViewHolder.tvTitle.setText(getListMovie().get(i).getTitle());
        nowShowingViewHolder.tvDescription.setText(getListMovie().get(i).getDescription());
        Glide.with(context)
                .load(getListMovie().get(i).getPosterUrl())
                .apply(new RequestOptions().override(Target.SIZE_ORIGINAL))
                .into(nowShowingViewHolder.imgPoster);
        nowShowingViewHolder.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Movie movieItems = getListMovie().get(i);
                Intent movieDetailsObjectIntent = new Intent(context, DetailsActivity.class);
                movieDetailsObjectIntent.putExtra(DetailsActivity.EXTRA_MOVIE_DETAILS, movieItems);
                movieDetailsObjectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(movieDetailsObjectIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public class NowShowingViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDescription;
        ImageView imgPoster;
        View convertView;

        public NowShowingViewHolder(@NonNull View itemView) {
            super(itemView);
            convertView = itemView;
            tvTitle = itemView.findViewById(R.id.tv_item_title);
            tvDescription = itemView.findViewById(R.id.tv_item_description);
            imgPoster = itemView.findViewById(R.id.img_item_poster);
        }
    }
}
